vim.opt.runtimepath:prepend (vim.fn.stdpath 'config' .. '/../nvim/')
vim.opt.runtimepath:append  (vim.fn.stdpath 'config' .. '/../nvim/after')
vim.opt.runtimepath:prepend (vim.fn.stdpath 'data' .. '/../nvim/site')
vim.opt.runtimepath:append  (vim.fn.stdpath 'data' .. '/../nvim/site/after')

dofile (vim.fn.stdpath 'config' .. '/../nvim/init.lua')

vim.cmd 'source ~/.vim/vimpagerrc'

vim.opt.modeline = false
vim.opt.foldmethod = 'manual'

vim.g.mapleader = [[\]]

vim.g.indentLine_enabled = 0
vim.g['deoplete#enable_at_startup'] = 0
vim.g.gutentags_dont_load = 1
vim.g.markdown_composer_autostart = 0

vim.api.nvim_create_autocmd('FileType', {
  pattern = 'org',
  callback = function ()
    vim.opt.foldmethod = 'manual'
  end
})
