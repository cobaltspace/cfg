
# IM {{{
export QT_IM_MODULES="wayland;fcitx;ibus"
export QT_IM_MODULE=fcitx
export GTK_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
export SDL_IM_MODULE=fcitx
# }}}
# Programs {{{
# export TERMINAL=footclient
export TERMINAL=alacritty
export EDITOR=nvim
export SUDO_EDITOR=$EDITOR
export VISUAL='neovide --no-tabs --no-fork'
export PAGER='bat'
export MANPAGER='bat -p'
# }}}
# Directories {{{
config_home="$(systemd-path user-configuration)"
data_home="$(systemd-path user-shared)"
cache_home="$(systemd-path user-state-cache)"
state_home="${XDG_STATE_HOME:-$HOME/.local/state}"
export GOPATH="$data_home/go"
export CARGO_HOME="$data_home/cargo"
export RUSTUP_HOME="$data_home/rustup"
export FFMPEG_DATADIR="$data_home/ffmpeg"
export AVCONV_DATADIR="$FFMPEG_DATADIR"
export GNUPGHOME="$data_home/gnupg"
export ICEAUTHORITY="$cache_home/ICEauthority"
export MPLAYER_HOME="$config_home/mplayer"
export NPM_CONFIG_USERCONFIG="$config_home/npmrc"
export PARALLEL_HOME="$config_home/parallel"
export SCREENRC="$config_home/screenrc"
export WINEPREFIX="$data_home/wineprefixes/default"
export DISTCC_DIR=/tmp/distcc
# export TEXMFCNF="$data_home/texmf:"
export TEXMFHOME="$data_home/texmf"
export TEXMFVAR="$cache_home/texlive/texmf-var"
export TEXMFCONFIG="$config_home/texlive/texmf-config"
export ASPELL_CONF="per-conf $config_home/aspell/aspell.conf; personal $config_home/aspell/en.pws; repl $config_home/aspell/en.prepl"
export GRADLE_USER_HOME="$data_home"/gradle
export KODI_DATA="$data_home"/kodi
export ANDROID_HOME="$data_home"/android
export CALCHISTFILE="$cache_home"/calc_history
export NUGET_PACKAGES="$cache_home"/NuGetPackages                                                             
export NVM_DIR="$data_home"/nvm
export PASSWORD_STORE_DIR="$data_home"/pass
export PYTHONSTARTUP="$config_home"/pythonrc
export STACK_ROOT="$data_home"/stack
export STACK_XDG=1
export GHCUP_USE_XDG_DIRS=1
export GTK2_RC_FILES="$config_home/gtk-2.0/gtkrc:$config_home/gtk-2.0/gtkrc.mine"
export LESSHISTFILE="$state_home"/less/history
export ERRFILE="$state_home"/X11/xsession-errors

export PATH="$(systemd-path user-binaries)":"$config_home/emacs/bin":"$CARGO_HOME/bin":"$GOPATH/bin":"$data_home/npm/bin":~/.dotnet/tools:$PATH
# }}}
# Settings {{{
# NVIM_LISTEN_ADDRESS     DEFAULT=/tmp/nvimsocket
export WINEDLLOVERRIDES="winemenubuilder.exe=d"
export SXHKD_SHELL=sh
# export MOZ_ENABLE_WAYLAND=1
export MOZ_USE_XINPUT2=1
# export MOZ_DBUS_REMOTE=1
export CMAKE_GENERATOR=Ninja
export SURVIVE_GLOBALSCENESOLVER=0
# export WLR_RENDERER=vulkan
# export WLR_NO_HARDWARE_CURSORS=1
export AMD_VULKAN_ICD=RADV
# export DISABLE_LAYER_AMD_SWITCHABLE_GRAPHICS_1=1
# }}}

export env_sources="${env_sources+$env_sources,}.config/environment.sh"

# vim:foldmethod=marker
