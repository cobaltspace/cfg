local myColorSchemes = {
  'kanagawabones',
  'GruvboxDark',
  'tokyonight',
}

return {
  enable_wayland = true,

  color_scheme = myColorSchemes[math.random(#myColorSchemes)],
  -- color_scheme = 'tokyonight',

  colors = { background = 'black' },

  window_background_opacity = 0.7,

  font = require('wezterm').font 'FantasqueSansM Nerd Font',
  default_cursor_style = 'BlinkingBlock',

  scrollback_lines = math.floor(2^17),

  default_cwd = '.',

  show_update_window = false,
  hide_tab_bar_if_only_one_tab = true,
  window_padding = {
    left = 0,
    right = 0,
    top = 0,
    bottom = 0,
  }
}
