config_home="$(systemd-path user-configuration)"
data_home="$(systemd-path user-shared)"
cache_home="$(systemd-path user-state-cache)"
fpath=($data_home/zsh/site-functions/ $fpath)
compinit -d $cache_home/zsh/zcompdump-$ZSH_VERSION
zstyle ':completion:*' cache-path $cache_home/zsh/zcompcache

# Enable Powerlevel10k instant prompt. Should stay close to the top of $config_home/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "$cache_home/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "$cache_home/p10k-instant-prompt-${(%):-%n}.zsh"
fi

bindkey -v

setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_REDUCE_BLANKS

if [ "$(lsb_release -is)" = Arch ]
then
  source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
  # source $data_home/powerlevel10k/powerlevel10k.zsh-theme
  source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

  source /usr/share/skim/key-bindings.zsh

  source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
  bindkey "$terminfo[kcuu1]" history-substring-search-up
  bindkey "$terminfo[kcud1]" history-substring-search-down
  bindkey -M vicmd 'k' history-substring-search-up
  bindkey -M vicmd 'j' history-substring-search-down
elif [ "$(lsb_release -is)" = Ubuntu ]
then
  source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

source $config_home/privaterc

# eval "$(fasd --init auto)"
eval "$(zoxide init zsh)"

zstyle ':completion:*' rehash true

source $config_home/aliases

compdef advcp=cp
compdef advmv=mv
compdef lsd=ls
compdef lvim=nvim

# compdef vr-theater=doas
compdef gamemoderun=exec
compdef gamescope=doas
compdef startgame=doas
compdef obs-gamecapture=exec
compdef obs-glcapture=obs-gamecapture
compdef obs-vkcapture=obs-gamecapture
compdef monado.sh=exec
compdef mangohud=exec
compdef vk_amdvlk=exec
compdef vk_pro=exec
compdef vk_radv=exec
compdef progl=exec

# To customize prompt, run `p10k configure` or edit $config_home/zsh/.p10k.zsh.
if [[ $TERM = linux ]]; then
  [[ ! -f $config_home/zsh/.p10k-tty.zsh ]] || source $config_home/zsh/.p10k-tty.zsh
else
  [[ ! -f $config_home/zsh/.p10k.zsh ]] || source $config_home/zsh/.p10k.zsh
fi

unset config_home data_home cache_home

export env_sources="${env_sources+$env_sources,}zshrc"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$('/opt/anaconda/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     eval "$__conda_setup"
# else
#     if [ -f "/opt/anaconda/etc/profile.d/conda.sh" ]; then
#         . "/opt/anaconda/etc/profile.d/conda.sh"
#     else
#         export PATH="/opt/anaconda/bin:$PATH"
#     fi
# fi
# unset __conda_setup
# <<< conda initialize <<<

