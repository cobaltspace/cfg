
# Guides to how to manage dot files

https://wiki.archlinux.org/index.php/Dotfiles

## A more detailed guide including how to clone to a new system

https://www.atlassian.com/git/tutorials/dotfiles

## Where this method came from

https://news.ycombinator.com/item?id=11070797

# TODO

## Install

### Shell

* `zsh`
* `grml-zsh-config`
* `zsh-syntax-highlighting`
* `zsh-autosuggestions`
* `zsh-history-substring-search`

### Display

* [`xorg`](https://www.archlinux.org/groups/x86_64/xorg/)

### Terminal

* `termite` (terminal emulator)
* `zsh-theme-powerlevel9k`
* `awesome-terminal-fonts`
* `otf-fantasque-sans-mono`

### Desktop Environment

* `bspwm`
* `sxhkd` (Hotkeys)
* `polybar` (Top bar)
* `compton` (compositing)
* `dex` (xdg autostart)
* `nitrogen` (wallpaper)
* `rofi` (launcher)
* `xbanish` (hide mouse cursor while typing)
* `redshift`

### Text Editor

* `gvim` (≥8)
* `neovim` (better command line vim)
* `python-neovim`
* `vimpager`
* `surf` (markdown preview)
* [`texlive-most`](https://www.archlinux.org/groups/x86_64/texlive-most/)
* [`texlive-lang`](https://www.archlinux.org/groups/x86_64/texlive-lang/)
* `biber`

### Audio

* `pulseaudio-alsa`
* `pulseaudio-bluetooth`
* `pulseaudio-jack`
* `pulseaudio-zeroconf`
* `pavucontrol`
* `pasystray`
* `jack2`
* [`pro-audio`](https://www.archlinux.org/groups/x86_64/pro-audio/) group from Arch Linux

### Input for Japanese

* `fcitx-gtk3`

### File manager

* `nemo`
* `vifm`
* `python-ueberzug` (images in vifm)

### Utilities from other desktops

* [`xfce4`](https://www.archlinux.org/groups/x86_64/xfce4/)
* [`xfce4-goodies`](https://www.archlinux.org/groups/x86_64/xfce4-goodies/)
* [`mate`](https://www.archlinux.org/groups/x86_64/mate/)
* [`mate-extra`](https://www.archlinux.org/groups/x86_64/mate-extra/)
* [`gnome`](https://www.archlinux.org/groups/x86_64/gnome/) (will go into finer detail later)
* [`gnome-extra`](https://www.archlinux.org/groups/x86_64/gnome-extra/) (will go into finer detail later)

### Internet Stuff

* `firefox`
* `chromium`
* `steam-native-runtime`
* `birdtray`
* `keybase-gui`
* `network-manager-applet`
* `jami-gnome`
* `itch`
* `electronmail-bin`
* `signal`
* `qbittorrent`
* `wire-desktop`
* `riot-desktop`
* `flatpak` Discord
* `flatpak` Bitwarden

### Key management

* `seahorse`

### VPN

* `nordvpn-bin`
* `wireguard-dkms`

### Other

* `udiskie`
* `colordiff`
* [`base-devel`](https://www.archlinux.org/groups/x86_64/base-devel/) (Arch Linux group)
* `pacman-contrib` (`checkupdates` for Arch Linux)

## Set up

* init all submodules

* set up `.private_profile` with First and Last Name, and Student ID
* set up `.privaterc`
* set up QjackCtl Scripts
