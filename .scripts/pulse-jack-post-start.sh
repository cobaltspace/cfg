#!/bin/sh
pactl load-module module-jack-sink channels=2
pactl load-module module-jack-source channels=1
pacmd set-default-sink jack_out
pacmd set-default-source jack_in
~/.scripts/pulse-jack-post-start-local.sh
