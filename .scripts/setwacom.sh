#!/bin/sh

xsetwacom set "Wacom Intuos S 2 Pen stylus" TabletPCButton on
xsetwacom set "Wacom Graphire2 4x5 Pen stylus" TabletPCButton on

for i in $(seq 1 9)
do
	xsetwacom set "Wacom Intuos S 2 Pad pad" Button $i 1$i
done

xsetwacom set "Wacom Intuos S 2 Pen stylus" Button 3 2
xsetwacom set "Wacom Intuos S 2 Pen stylus" Button 2 3

touch /tmp/precise-wacom-token
xprecisewacom a
