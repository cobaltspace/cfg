#! /bin/sh
# A sh script for an advanced setup of a Wacom on Linux :
# with a grep, automatic parsing of the Wacom identifier, of the screen, of dpi and with a precision mode
# ( drawing at 1:1 scale , the tablet / the screen ) .
# Only the button layout remain custom to the model ( Intuos 3 in this example )
# and can be easily adapted with other buttons ID.
#
# Dependencies: libwacom (xsetwacom), sh and bc for the math, xrandr, xdotool
#               optional: Gnome icon, notify-send
#               ( tested/created on Mint 17.2 Cinnamon, 11/2015 )
#
# Usage: Edit the script to enter the real world size of your tablet active zone (around line 30),
#        Edit the script to set your tablet and stylus's name ( around line 47),
#        Execute the script to setup the tablet. It's a toggle:
#        1. First launch, tablet will be mapped fullscreen, with ratio correction.
#        2. Second launch, tablet will setup a precision mode.
#        3. Third launch loop to 1 , etc... a toggle.
#
# License: GPLv3 or later
# author: Cobalt Space
# original author: www.peppercarrot.com


# Configuration information
# Enter here the active area of your tablet in centimeters (mesure in physical world) :
XtabletactiveareaCM=15.2
YtabletactiveareaCM=9.5
# Custom preference
# Correction scaling can enlarge a bit the precision zone.
# I saw during test a precision zone slightly larger has no impact on control quality, and might feels better.
# default=1 for real precision zone, enlarge=1.5 or reduce=0.8
# myfav: slighly-larger=1.11 with Intuos3 A4 and 96dpi 21inch 1080p workstation screen.
#        larger=1.5 with Intuos4 Medium and 120dpi 15inch 1080p laptop screen.
correctionscalefactor=1


# Cursor position
CursorX=$(xdotool getmouselocation --shell | grep X= | sed s/X=//)
CursorY=$(xdotool getmouselocation --shell | grep Y= | sed s/Y=//)

# Under this line, everything should be automatic, exept customising your buttons
# Tablet
tabletstylus="Wacom Intuos S 2 Pen stylus"
# tableteraser=$(xsetwacom --list | grep ERASER | cut -d ' ' -f 1-5 | sed -e 's/[[:space:]]*$//')
tabletpad="Wacom Intuos S 2 Pad pad"
xsetwacom --set "$tabletstylus" ResetArea
# xsetwacom --set "$tableteraser" ResetArea
fulltabletarea=`xsetwacom get "$tabletstylus" Area | grep "[0-9]\+ [0-9]\+$" -o`
Xtabletmaxarea=`echo $fulltabletarea | grep "^[0-9]\+" -o`
Ytabletmaxarea=`echo $fulltabletarea | grep "[0-9]\+$" -o`

# Screen
Xscreenpix=$(xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f1 | head -n1)
Yscreenpix=$(xrandr --current | grep '*' | uniq | awk '{print $1}' | cut -d 'x' -f2 | head -n1)
screenPPI=$(xdpyinfo | grep dots | awk '{print $2}' | awk -Fx '{print $1}')
XscreenPPI=$(echo "scale = 2; $Xscreenpix / $screenPPI" | bc)
YscreenPPI=$(echo "scale = 2; $Yscreenpix / $screenPPI" | bc)
XscreenCM=$(echo "scale = 0; $Xscreenpix * 0.0254" | bc)
YscreenCM=$(echo "scale = 0; $Yscreenpix * 0.0254" | bc)

# Precise Mode + Ratio
Ytabletmaxarearatiosized=$(echo "scale = 0; $Yscreenpix * $Xtabletmaxarea / $Xscreenpix" | bc)
XtabletactiveareaPIX=$(echo "scale = 0; $XtabletactiveareaCM * $screenPPI / 2.54 * $correctionscalefactor" | bc)
YtabletactiveareaPIX=$(echo "scale = 0; $YtabletactiveareaCM * $screenPPI / 2.54 * $correctionscalefactor" | bc)
XtabletactiveareaPIX=$(echo "scale = 0; ($XtabletactiveareaPIX + 0.5) / 1" | bc)
YtabletactiveareaPIX=$(echo "scale = 0; ($YtabletactiveareaPIX + 0.5) / 1" | bc)
XOffsettabletactiveareaPIX=$(echo "($Xscreenpix - $XtabletactiveareaPIX) * $CursorX / ($Xscreenpix - 1)" | bc -l)
YOffsettabletactiveareaPIX=$(echo "($Yscreenpix - $YtabletactiveareaPIX) * $CursorY / ($Yscreenpix - 1)" | bc -l)
XOffsettabletactiveareaPIX=$(echo "scale = 0; $XOffsettabletactiveareaPIX / 1" | bc)
YOffsettabletactiveareaPIX=$(echo "scale = 0; $YOffsettabletactiveareaPIX / 1" | bc)

# Verbose for debugging
echo "Setup script for your $tabletpad"
echo "-----------------------------------------"
echo ""
echo "Debug informations:"
echo "Tablet size (cm) :" "$XtabletactiveareaCM" x "$YtabletactiveareaCM"
echo "Screen size (px) :" "$Xscreenpix" x "$Yscreenpix"
echo "Screen size (cm) :" "$XscreenCM" x "$YscreenCM"
echo "Screen ppi :" "$screenPPI"
echo "Correction factor :" "$correctionscalefactor"
echo "Maximum tablet-Area (Wacom unit):" "$Xtabletmaxarea" x "$Ytabletmaxarea"
echo "Precision-mode area (px):" "$XtabletactiveareaPIX" x "$YtabletactiveareaPIX"
echo "Precision-mode offset (px):" "$XOffsettabletactiveareaPIX" x "$YOffsettabletactiveareaPIX"


# INTUOS 3 Large Infos:
# max area : 0 0 60960 45720
# ---------
# |   | 1 |
# | 3 |---|
# |   | 2 |
# |-------|
# |   8   |
# ---------
xsetwacom set "$tabletstylus" RawSample 4

# In case of Nvidia gfx:
xsetwacom set "$tabletstylus" MapToOutput "HEAD-0"
# xsetwacom set "$tableteraser" MapToOutput "HEAD-0"
xsetwacom set "$tabletpad" MapToOutput "HEAD-0"

# Precision mode start here :
# Dual configuration
if [ -f /tmp/wacomscript-memory-tokken ]; then
  # Here Precision mode; full tablet area in cm are 1:1 on a portion of the screen.
  echo "Precision mode"
  xsetwacom set "$tabletstylus" Area 0 0 "$Xtabletmaxarea" "$Ytabletmaxarea"
  # xsetwacom set "$tableteraser" Area 0 0 "$Xtabletmaxarea" "$Ytabletmaxarea"
  xsetwacom set "$tabletstylus" MapToOutput "$XtabletactiveareaPIX"x"$YtabletactiveareaPIX"+"$XOffsettabletactiveareaPIX"+"$YOffsettabletactiveareaPIX"
  notify-send -i /usr/share/icons/gnome/22x22/devices/input-tablet.png "Precision mode" "$XtabletactiveareaPIX x $YtabletactiveareaPIX part-of-screen"
  rm /tmp/wacomscript-memory-tokken
else
  # Here normal mode; tablet map to Fullscreen with ratio correction
  echo "Full-screen mode with ratio correction"
  xsetwacom set "$tabletstylus" Area 0 0 "$Xtabletmaxarea" "$Ytabletmaxarearatiosized"
  # xsetwacom set "$tableteraser" Area 0 0 "$Xtabletmaxarea" "$Ytabletmaxarearatiosized"
  xsetwacom set "$tabletstylus" MapToOutput "$Xscreenpix"x"$Yscreenpix"+0+0
  notify-send -i /usr/share/icons/gnome/22x22/devices/input-tablet.png "Normal mode" "full-screen"
  touch /tmp/wacomscript-memory-tokken
fi
