#!/bin/sh

STATUS=$(nordvpn status | grep Status | tr -d ' ' | cut -d ':' -f2)

FIELD='City'

if [ "$STATUS" = "Connected" ]; then
    nordvpn status | grep "$FIELD" | sed "s/$FIELD: //" | sed "s/.nordvpn.com//"
else
    echo " "
fi
