bspc subscribe node_focus | while read wid; do
  wid=$(echo $wid | cut -d" " -f4)
  class="$(xprop -id $wid '\n$1\n' WM_CLASS | tail -n1)"
  instance="$(xprop -id $wid '\n$0\n' WM_CLASS | tail -n1)"
  name="$(xprop -id $wid '\n$0\n' WM_NAME | tail -n1)"

  # echo $resetcmd
  $resetcmd
  resetcmd=''
  # [ -n "$tokill" ] && kill $tokill && unset tokill
  case "$class" in
  '"Com.github.xournalpp.xournalpp"')

    sxhkd -c ~/.config/sxhkd/xournalpp &
    resetcmd="kill $!"
    ;;
  '"itunes.exe"')
    [ "$name" = "<field not available>" ] && bspc config focus_follows_pointer false && resetcmd='bspc config focus_follows_pointer true'
    ;;
  esac
done
